var express = require('express');
var router = express.Router();

var AvailabilityService = require('./../services/AvailabilityService');
var availabilityService = new AvailabilityService();
var ResponseHandler = require('./../handlers/ResponseHandler');



router.get('/',function(req, res, next) {
    availabilityService.getAvailability(req,function (all_availability, err) {
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, all_availability);
        }
      });
});



router.put('/set',function(req, res, next) {
    availabilityService.setAvailability(req,function (set_availability, err) {
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, set_availability);
        }
      });
});



module.exports = router;