var express = require('express');
var router = express.Router();

var CallHistoryService = require('./../services/CallHistoryService');
var callHistoryService = new CallHistoryService();
var ResponseHandler = require('./../handlers/ResponseHandler');



router.get('/user',function(req, res, next) {
    callHistoryService.getcallHistoryByUser(req,function (user, err) {
      if(req.query.merchant_phone == undefined || req.query.user_phone ==undefined){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, user);
        }
      });
});



router.get('/all',function(req, res, next) {
    callHistoryService.getcallHistory(req,function (user, err) {
      if(req.query.merchant_phone == undefined ){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, user);
        }
      });
});

router.get('/last',function(req, res, next) {
  callHistoryService.getLastCall(req,function (user, err) {
    if(req.query.merchant_phone == undefined){
      ResponseHandler.handleBadRequestQueryParam(res);
      return;
  }
      if (err) {
        console.error(err);
        ResponseHandler.handleServerError(res);
      }
      else {
        ResponseHandler.handleSuccess(res, user);
      }
    });
});

module.exports = router;