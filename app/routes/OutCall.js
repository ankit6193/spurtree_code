var express = require('express');
var router = express.Router();

var OutCallService = require('./../services/OutCallService');
var outCallService = new OutCallService();
var ResponseHandler = require('./../handlers/ResponseHandler');



router.get('/',function(req, res, next) {
    outCallService.makeACall(req,function (user, err) {
      if(req.query.exoPhone == undefined || req.query.dialTo == undefined || req.query.appUrlId == undefined ){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }

        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, user);
        }
      });
});







module.exports = router;