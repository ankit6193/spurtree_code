var express = require('express');
var router = express.Router();

var SendOTPService = require('./../services/SendOTPService');
var sendOTPService = new SendOTPService();
var ResponseHandler = require('./../handlers/ResponseHandler');

var QueryValidator = require('./../validator/queryValidator');
var queryValidator = new QueryValidator();

// router.get('/',function(req, res, next) {
//     var result = {
//         message:"Response from Api Connect DB to test other api's"
//       }
//         res.send(result)
// });

// Get call to know if the user is Open/Close or its a Self call

router.get('/',function(req, res, next) {
    if(req.query.mobile_number == undefined || req.query.appUrlId == undefined ){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
    sendOTPService.sendOTP(req,function (send_otp, err) {
        
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
             ResponseHandler.handleSuccess(res, send_otp);
        }
        
      });
});

module.exports = router;