const db = require('../models/models');

function Logs() {
}


Logs.prototype.InsertLogs = function(req, res, next){
  var my_pay_load;
  if(Object.keys(req.query).length === 0){
    my_pay_load = JSON.stringify(req.body)
  }else{
    my_pay_load = JSON.stringify(req.query)
  }
  var data = {
      "requesturl": req.originalUrl,
      "request_payload":my_pay_load,
  }
    db.tpt_api_log.create(data,{
    }).then(next())
    next();
}

Logs.prototype.UpdateLogs = function(req, res){
    var data = {
        "response":JSON.stringify(res),
        
    }
      db.tpt_api_log.update(data,{
        where:{requesturl:req}
    })
  }
module.exports = Logs;