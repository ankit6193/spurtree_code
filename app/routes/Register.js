var express = require('express');
var router = express.Router();

var RegisterService = require('./../services/RegisterService');
var registerService = new RegisterService();
var ResponseHandler = require('./../handlers/ResponseHandler');

var QueryValidator = require('./../validator/queryValidator');
var queryValidator = new QueryValidator();
router.get('/',function(req, res, next) {
    var result = {
        message:"Response from Api Connect DB to test other api's"
      }
        res.send(result)
});


router.post('/',function(req, res, next) {
    if(req.body.full_name == undefined || req.body.company_name == undefined || req.body.email == undefined || req.body.merchant_phone == undefined || req.body.address == undefined  || req.body.telco_region_code == undefined || req.body.appUrlId==undefined || req.body.platform==undefined || req.body.device_id==undefined){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }

    registerService.registermerchants(req,function (register_merchants, err) {
        
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
             ResponseHandler.handleSuccess(res, register_merchants);
        }
        
      });
});


module.exports = router;