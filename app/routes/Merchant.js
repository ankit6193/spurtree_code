var express = require('express');
var router = express.Router();

var MerchantService = require('./../services/MerchantService');
var merchantService = new MerchantService();
var ResponseHandler = require('./../handlers/ResponseHandler');



router.get('/:id',function(req, res, next) {
    merchantService.getMerchantById(req,function (user, err) {
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, user);
        }
      });
});







module.exports = router;