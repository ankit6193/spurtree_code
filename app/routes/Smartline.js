var express = require('express');
var router = express.Router();

var SmartlineService = require('./../services/SmartlineService');
var smartlineService = new SmartlineService();
var ResponseHandler = require('./../handlers/ResponseHandler');

var QueryValidator = require('./../validator/queryValidator');
var queryValidator = new QueryValidator();
router.get('/',function(req, res, next) {
    var result = {
        message:"Response from Api Connect DB to test other api's"
      }
        res.send(result)
});

// Get call to know if the user is Open/Close or its a Self call

router.get('/getcallstrategy',function(req, res, next) {
    if(req.query.CallSid == undefined || req.query.From == undefined || req.query.To == undefined ){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
    // if(req.query.from == undefined){
    //     ResponseHandler.handleBadRequestQueryParam(res,"Please provide from number");
    //     return;
    // }
    // if(req.query.to == undefined){
    //     ResponseHandler.handleBadRequestQueryParam(res,"Please provide to number");
    //     return;
    // }
    // if(req.query.direction == undefined){
    //     ResponseHandler.handleBadRequestQueryParam(res,"Please provide direction");
    //     return;
    // }
    // if(req.query.current_time == undefined){
    //     ResponseHandler.handleBadRequestQueryParam(res,"Please provide current_time");
    //     return;
    // }
   
    smartlineService.getcallstrategy(req,function (call_strategy, err) {
        
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
             ResponseHandler.customHandleSuccess(res, call_strategy);
        }
        
      });
});

//Get call to get the greeting of the merchant to be played

router.get('/getgreetingfile',function(req, res, next) {
    if(req.query.Type == undefined ||req.query.CallSid == undefined || req.query.From == undefined || req.query.To == undefined || req.query.Direction == undefined || req.query.CurrentTime == undefined){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
    
    smartlineService.getgreetingfile(req,function (call_greetings, err) {
        
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
            
             ResponseHandler.customHandleSuccess(res, call_greetings);
        }
        
      });
});



//Get call to get the nuber to be dialed

router.get('/getnumbertodial',function(req, res, next) {
    if(req.query.CallSid == undefined || req.query.From == undefined || req.query.To == undefined || req.query.Direction == undefined || req.query.CurrentTime == undefined){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
    
    smartlineService.getnumbertodial(req,function (number_to_dial, err) {
        
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
             ResponseHandler.customHandleSuccess(res, number_to_dial);
        }
        
      });
});



    

//Get call to enter/update the call log

router.get('/recordcallinfo',function(req, res, next) {
    if(req.query.CallType == undefined ||req.query.CallSid == undefined || req.query.From == undefined || req.query.To == undefined || req.query.Direction == undefined || req.query.CurrentTime == undefined || req.query.DialCallDuration == undefined || req.query.StartTime == undefined || req.query.EndTime == undefined )
        {
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
    }
    
    smartlineService.recordcallinfo(req,function (number_to_dial, err) {
        
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
             ResponseHandler.customHandleSuccess(res, number_to_dial);
        }
        
      });
});
module.exports = router;