
var express = require('express');
var router = express.Router();
var request = require('request');
var multer  = require('multer')
var Lame = require("node-lame").Lame;
var fs = require("fs");

var AWS = require('aws-sdk');
var multerS3 = require('multer-s3');

AWS.config.loadFromPath("config/s3.json");// '/aws-cred/s3.json'

var GreetingsService = require('./../services/GreetingsService');
var greetingsService = new GreetingsService();
var ResponseHandler = require('./../handlers/ResponseHandler');



router.get('/',function(req, res, next) {
    greetingsService.getGreetings(req,function (user, err) {
      if(req.query.merchant_phone == undefined  ){
        ResponseHandler.handleBadRequestQueryParam(res);
        return;
      }
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, user);
        }
      });
});

router.delete('/delete',function(req, res, next) {
  greetingsService.deleteGreetings(req,function (user, err) {
    if(req.query.merchant_phone == undefined || req.query.greeting_type == undefined ){
      ResponseHandler.handleBadRequestQueryParam(res);
      return;
    }
      if (err) {
        console.error(err);
        ResponseHandler.handleServerError(res);
      }
      else {
        ResponseHandler.handleSuccess(res, user);
      }
    });
});


var filename;
var filePath = "uploads/";
var storage_local = multer.diskStorage({
  destination: function (req, file, cb) {
    
    cb(null, filePath)
  },
  filename: function (req, file, cb) {
    // console.log("*******************************",file.mimetype,file.originalname.split(".")[1]);
    filename = req.query.type +Date.now();
    cb(null, filename)
  }
})


var s3 = new AWS.S3({ Bucket: "godaddy-exotel" });

router.post('/set',function (req, res, next){


  var is_done = false;
  var upload = multer({storage: storage_local }).single('file');
    upload (req,res,function(err){
    if(err){
      console.log(err);
    }else{
      const encoder = new Lame({
        "output": "uploads/"+filename+".mp3",
       "bitrate": 192
       }).setFile("uploads/"+filename);
          
       encoder.encode()
       .then(() => {
        fs.unlink("uploads/"+filename);
        if(req.query.merchant_phone == undefined){
          ResponseHandler.handleBadRequestQueryParam(res);
          return;
        }else if( req.query.type != "ON" && req.query.type != "OFF"){
          ResponseHandler.handleBadRequestQueryParam(res);
          return;
        }else{   
          
              greetingsService.setGreetings(req,filename,function (user, err) {
                if (err) {
                  console.error(err);
                  ResponseHandler.handleServerError(res);
                }
                else {
                  var my_files;
                 
                    
                      var formData = {file:fs.createReadStream('uploads/'+filename+'.mp3')}
                      request.post({url:'http://localhost:9000/smartline/mobile/greetings/upload', formData: formData},
                      function optionalCallback(err, httpResponse, body) {
                      if (err) {
                        ResponseHandler.handleServerError(res);
                      }
                      fs.unlink("uploads/"+filename+".mp3");
                      console.log('Upload successful!  Server responded with:', body);
                      });
                   
                  ResponseHandler.handleSuccess(res, user);
                }
              });
        }
       })
       .catch((error) => {
       console.log("******************",error)
       });
    }
  });
  // setTimeout(function(){
  //   if(is_done){
      
  //   } 
  // }, 2000);      
});


router.post('/upload',function(req, res, next) {
      var storage = multerS3({
      s3: s3,
      acl: 'public-read-write',
      bucket: "godaddy-exotel",
      metadata: function (req, file, cb) {
      cb(null, { fieldName:'file'});
      },
      key: function (req, file, cb) {
      cb(null, file.originalname)
      }
      })

      
      var upload = multer({storage: storage }).single('file');
      upload(req,res,function(err){
        if(err){
          console.log("S3 upload error",err);
          ResponseHandler.handleServerError(res);
          return
        }else{
          ResponseHandler.handleSuccess(res);
        }
      })


     
    
});


module.exports = router;