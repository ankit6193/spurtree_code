var express = require('express');
var router = express.Router();

var TelcoCirclesService = require('./../services/TelcoCirclesService');
var telcoCirclesService = new TelcoCirclesService();
var ResponseHandler = require('./../handlers/ResponseHandler');



router.get('/',function(req, res, next) {
    telcoCirclesService.getCircles(function (user, err) {
        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
          ResponseHandler.handleSuccess(res, user);
        }
      });
});







module.exports = router;