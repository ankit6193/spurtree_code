var express = require('express');
var router = express.Router();

var ValidateOTPService = require('./../services/ValidateOTPService');
var validateOTPService = new ValidateOTPService();
var ResponseHandler = require('./../handlers/ResponseHandler');

var QueryValidator = require('./../validator/queryValidator');
var queryValidator = new QueryValidator();

router.get('/',function(req, res, next) {
    validateOTPService.validateOTP(req,function (validate_otp, err) {
        if(req.query.mobile_number == undefined || req.query.otp == undefined || req.query.appUrlId == undefined ){
            ResponseHandler.handleBadRequestQueryParam(res);
            return;
        }

        if (err) {
          console.error(err);
          ResponseHandler.handleServerError(res);
        }
        else {
             ResponseHandler.handleSuccess(res, validate_otp);
        }
        
      });
});

module.exports = router;