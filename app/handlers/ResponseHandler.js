var util = require('util');
var http = require('http');
var fs = require('fs');
var request = require('request');
var RESTErrorMessages = require('./../constants/RESTErrorMessages');

var Logs = require('../routes/Logs')
var logs = new Logs();


function ResponseHandler() {

}

function ResponseBody() {
    this.status = "failure";
    this.origin = "";
    this.executionTime = "";
    this.uid = "";
    this.sessionid = "";
    this.count = null;
    this.error = null;
}

ResponseHandler.handleSuccess = function (res, data) {
    var response = new ResponseBody();
    response.status = "success";
    response.result = data;
    if (util.isArray(data)) {
        response.count = data.length;
    }
    else {
        //Data must be an object, Hence setting count as 1
        response.count = 1;
    }
    var x = res.status().req.originalUrl
    logs.UpdateLogs(x,response)
    res.status(200).send(response);
    
}

ResponseHandler.customHandleSuccess = function (res, data) {
    res.setHeader('content-type', 'text/plain');
    res.status(200).send(data);
}



// ResponseHandler.mcustomHandleSuccess = function (res, data) {
//     // request.get(data)
//     // .on('response', response => {
//     //   res.setHeader('Content-Type', 'audio/mpeg');

//     //   // pipe response to res 
//     //   // since response is an http.IncomingMessage
//     //   response.pipe(res);
//     // });
//     // var host = req.get(data);
//     res.redirect(301, data);
//     // res.status(200).send(data)
// }

ResponseHandler.handleConflictResourceWithMsg = function (res, msg) {
    var response = new ResponseBody();
    response.status = "error";
    response.message = msg   
    res.status(409).send(response); 
}

ResponseHandler.handleBadRequestQueryParam = function (res) {
    var response = new ResponseBody();
    response.status = "failure";
    response.message = RESTErrorMessages.MissingRequiredQueryParam;
    res.status(400).send(response);
}

ResponseHandler.handleBadRequestBody = function (res) {
    var response = new ResponseBody();
    response.message = RESTErrorMessages.MissingRequiredBody;
    res.status(400).send(response);
}

ResponseHandler.handleBadRequestBodyWithErrors = function (res, error) {    
    var response = new ResponseBody();
    response.message = RESTErrorMessages.MissingRequiredBody;
    response.error = error;
    res.status(400).send(response);
}

ResponseHandler.handleResourceNotFound = function (res) {
    var response = new ResponseBody();
    response.message = RESTErrorMessages.ResourceNotFound;
    res.status(404).send(response);
}

ResponseHandler.handleServerError = function (res) {
    var response = new ResponseBody();
    response.message = RESTErrorMessages.InternalError;
    res.status(500).send(response);
}

ResponseHandler.handleUnAuthorized = function (res) {
    var response = new ResponseBody();
    response.message = RESTErrorMessages.Unauthorized;
    res.status(401).send(response);
}

module.exports = ResponseHandler;