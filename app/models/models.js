var config = require('config');
var mysqlConfig = config.get('mysql');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(mysqlConfig.database, mysqlConfig.username, mysqlConfig.password, {
    host: mysqlConfig.host,
    dialect: mysqlConfig.dialect,
    pool: {
        min: mysqlConfig.min,
        max: mysqlConfig.max,
        idle: mysqlConfig.idle
    }
});


// var roles = sequelize.define('roles', {
//     roles: { type: Sequelize.STRING(45) },
// }, {
//         underscored: true
//     });

var merchant = sequelize.define('merchant', {
    merchant_name: { type: Sequelize.STRING(45) },
    merchant_address: { type: Sequelize.STRING(45) },
    telco_region_code : { type: Sequelize.STRING(45) },
    email : { type: Sequelize.STRING(45) },
    company_name : { type: Sequelize.STRING(45) },
    app_id: { type: Sequelize.INTEGER(45) },
    phone: { type: Sequelize.STRING(45) },
    created_at: { type: Sequelize.TIME(45) },
    updated_at: { type: Sequelize.TIME(45) },
}, {
        underscored: true,
        freezeTableName: true
    });

var merchant_schedule = sequelize.define('merchant_schedule', {
   
    dayoftheweek: { type: Sequelize.INTEGER(11) },
    is_available: { type: Sequelize.INTEGER(11) },
    start_hour: { type: Sequelize.INTEGER(11) },
    start_min: { type: Sequelize.INTEGER(11) },
    end_hour: { type: Sequelize.INTEGER(11) },
    end_min: { type: Sequelize.INTEGER(11) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });
merchant_schedule.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasOne(merchant_schedule)
var merchant_contacts = sequelize.define('merchant_contacts', {
    merchant_id: { type: Sequelize.INTEGER(11) },
    contact_phone: { type: Sequelize.DOUBLE(11) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });

merchant_contacts.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasOne(merchant_contacts)

var merchant_greetings = sequelize.define('merchant_greetings', {
    merchant_id: { type: Sequelize.INTEGER(11) },
    greeting_type: { type: Sequelize.STRING(11) },
    greeting_url: { type: Sequelize.STRING(11) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });

merchant_greetings.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasOne(merchant_greetings)

var call_master = sequelize.define('call_master', {
    callSid: { type: Sequelize.INTEGER(11) },
    merchant_id: { type: Sequelize.INTEGER(11) },
    contact_id: { type: Sequelize.INTEGER(11) },
    is_outgoing: { type: Sequelize.INTEGER(11) },
    request_status: { type: Sequelize.STRING(11) },
    call_status: { type: Sequelize.STRING(11) },
    recording_url: { type: Sequelize.STRING(200) },
    status_id: { type: Sequelize.INTEGER(11) },
    call_initiation_time: { type: Sequelize.INTEGER(11) },
    call_completion_time: { type: Sequelize.INTEGER(11) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });

call_master.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasMany(call_master)




var otp_validator = sequelize.define('otp_validator', {
    phone: { type: Sequelize.STRING(11) },
    otp: { type: Sequelize.STRING(11) },
    validationstatus: { type: Sequelize.INTEGER(11) },
    authtoken: { type: Sequelize.STRING(200) },
    added_at: { type: Sequelize.TIME(11) },
    app_id: { type: Sequelize.INTEGER(11) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });


var telco_circle = sequelize.define('telco_circle', {
    name: { type: Sequelize.STRING(11) },
    code: { type: Sequelize.STRING(11) }
}, {
        underscored: true,
        freezeTableName: true
    });


var second_lines = sequelize.define('second_lines', {
    secondline: { type: Sequelize.STRING(200) },
    merchant_id: { type: Sequelize.INTEGER(11) },
    status :{ type: Sequelize.STRING(200) },
    telco_circle_id :{ type: Sequelize.INTEGER(11) },
}, {
        underscored: true,
        freezeTableName: true
    });
second_lines.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasOne(second_lines)

var merchant_device = sequelize.define('merchant_device', {
    merchant_id: { type: Sequelize.INTEGER(11) },
    platform :{ type: Sequelize.STRING(200) },
    device_id :{ type: Sequelize.INTEGER(11) },
}, {
        underscored: true,
        freezeTableName: true
    });
merchant_device.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasOne(merchant_device)

var call_history_log = sequelize.define('call_history_log', {
    CallSid: { type: Sequelize.INTEGER(200) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at :{ type: Sequelize.TIME(200) },
    From :{ type: Sequelize.STRING(11) },
    To :{ type: Sequelize.STRING(11) },
    Direction :{ type: Sequelize.STRING(11) },
    StartTime :{ type: Sequelize.TIME(11) },
    EndTime :{ type: Sequelize.TIME(11) },
    CurrentTime :{ type: Sequelize.TIME(11) },
    CallType :{ type: Sequelize.STRING(11) },
    RecordingUrl :{ type: Sequelize.STRING(200) },
    DialCallDuration:{ type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });
call_history_log.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasMany(call_history_log)

var app_identifier = sequelize.define('app_identifier', {
    app_url_id: { type: Sequelize.STRING(200) },
    app_name: { type: Sequelize.STRING(11) },
    app_sid: { type: Sequelize.STRING(11) },
    app_token: { type: Sequelize.STRING(11) },
    flow_id: { type: Sequelize.TIME(11) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });


var tpt_api_log = sequelize.define('tpt_api_log', {
    merchant_id: { type: Sequelize.INTEGER(11) },
    requesturl: { type: Sequelize.STRING(11) },
    request_payload: { type: Sequelize.STRING(5000) },
    response: { type: Sequelize.STRING(5000) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });

tpt_api_log.belongsTo(merchant,{foreignKey:'merchant_id',targetKey:'id'})
merchant.hasMany(tpt_api_log)


var call_status_display = sequelize.define('call_status_display', {
    id: { type: Sequelize.INTEGER(11),primaryKey: true },
    status_name : { type: Sequelize.STRING(100) },
    direction : { type: Sequelize.STRING(100) },
    incoming_status : { type: Sequelize.STRING(100) },
    created_at: { type: Sequelize.TIME(11) },
    updated_at: { type: Sequelize.TIME(11) },
}, {
        underscored: true,
        freezeTableName: true
    });


    call_master.belongsTo(call_status_display,{foreignKey:'status_id',targetKey:'id'})


module.exports.sequelize = sequelize;
// module.exports.roles = roles;
module.exports.merchant = merchant;
module.exports.merchant_schedule = merchant_schedule;
module.exports.merchant_contacts = merchant_contacts
module.exports.merchant_greetings = merchant_greetings;
module.exports.call_master = call_master;
module.exports.otp_validator = otp_validator;
module.exports.telco_circle = telco_circle;
module.exports.second_lines = second_lines;
module.exports.app_identifier = app_identifier;
module.exports.merchant_device = merchant_device;
module.exports.call_history_log = call_history_log;
module.exports.tpt_api_log = tpt_api_log;
module.exports.call_status_display = call_status_display;

