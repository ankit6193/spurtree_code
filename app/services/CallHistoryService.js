var CallHistorytDAO = require('./../dao/CallHistoryDAO');
var callHistorytDAO = new CallHistorytDAO();

var MerchantDAO = require('./../dao/MerchantDAO');
var merchantDAO = new MerchantDAO();

function CallHistoryService() {

}

CallHistoryService.prototype.getcallHistoryByUser = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone,function(merchant_details,err){
        if(merchant_details){
            var data = {
                "merchant_id":merchant_details.merchant_id,
                "user_phone":req.query.user_phone
            }
            
            callHistorytDAO.getcallHistoryByUser(data,function(call_history_response,err){
                    var response = {"validate_status":1,"call_history":call_history_response[0]};
                    callback(response,err);
               
            });
        }else{
            call_history_response = {"validate_status":0,"message":"Merchant not found"}
            callback(call_history_response,err)
        }   
    });
}

CallHistoryService.prototype.getcallHistory = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone,function(merchant_details,err){
        if(merchant_details){
            var data = {
                "merchant_id":merchant_details.merchant_id
            }
            callHistorytDAO.getcallHistory(data,function(call_history_response,err){
                my_response = call_history_response[0]
                
                    var response = {"validate_status":1,"call_history":my_response};
                    callback(response,err);
                
            });
        }else{
            call_history_response = {"validate_status":0,"message":"Merchant not found"}
            callback(call_history_response,err)
        }
    });
}

CallHistoryService.prototype.getLastCall = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone,function(merchant_details,err){
        if(merchant_details){
            var data = {
                "merchant_id":merchant_details.merchant_id
            }
            callHistorytDAO.getLastCall(data,function(call_history_response,err){
                var response = {"validate_status":1,"call_history":call_history_response}
                callback(response,err)
            });
        }else{
            call_history_response = {"validate_status":0,"message":"Merchant not found"}
            callback(call_history_response,err)
        }
    });
}

module.exports = CallHistoryService;