var RegisterDAO = require('./../dao/RegisterDAO');
var registerDAO = new RegisterDAO();

var GetExoPhoneService = require('../services/GetExoPhoneService');
var getExoPhoneService = new GetExoPhoneService();

var MerchantService = require('./MerchantService');
var merchantService = new MerchantService();

var MerchantDAO = require('./../dao/MerchantDAO');
var merchantDAO = new MerchantDAO();
var request = require('request');

function RegisterService() {

}

RegisterService.prototype.registermerchants = function (req,callback) {
    merchantService.getAppId(req.body.appUrlId,function(appDetails,err){
        if(appDetails == null){
            register_response = {"validate_status":0,"message":"Not a valid appURL ID"}
            callback(register_response,err);
        }else{
            merchantDAO.getMerchantByPrivatePhone(req.body.merchant_phone,function(merchant_exist,err){
                if(merchant_exist == null){
                    var my_exo_phone = "";
                    getExoPhoneService.getMyExo(req.body.telco_region_code,function(res){
                        my_exo_phone = res;
                        console.log(my_exo_phone);
                        var data = {
                            "merchant_name": req.body.full_name,
                            "merchant_address":req.body.address,
                            "secondline": my_exo_phone,
                            "phone": req.body.merchant_phone,
                            "telco_region_code":req.body.telco_region_code,
                            "email": req.body.email,
                            "company_name": req.body.company_name,
                            "app_id" : appDetails.id,
                            "platform":req.body.platform,
                            "device_id": req.body.device_id,
                        }
                        if(my_exo_phone.statusCode){
                            response = {"validate_status":0,"message":"Not Register" ,"error":my_exo_phone};
                            callback(response,err)
                        }else{
                            registerDAO.registermerchants(data,function(registered_merchant,err){
                                data.id = registered_merchant.id
                                registerDAO.addSchedules(data.id)
                                registerDAO.addmerchantsSecondLine(data)
                                registerDAO.addmerchantsDevice(data)
                                registered_merchant.dataValues.exo_phone = my_exo_phone;
                                registered_merchant.dataValues.validate_status = 1;
                                var registered_merchant_response = registered_merchant;
                                callback(registered_merchant_response,err)
                            });
                        }
                        
                    })        
        
                }else{
                    var register_merchant_response = {"validate_status":0,"message": "Registration Failed Merchant Already exits"}
                    callback(register_merchant_response,err)
                }
                
                // registerDAO.registermerchants(req,callback);
            });      
        }
    })
}



module.exports = RegisterService;