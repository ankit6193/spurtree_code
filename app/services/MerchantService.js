var MerchantDAO = require('./../dao/MerchantDAO');
var merchantDAO = new MerchantDAO();

function MerchantService() {

}

MerchantService.prototype.getMerchantById = function (req,callback) {
    merchantDAO.getMerchantById(req,callback);
}

MerchantService.prototype.getMerchantByPhone = function (phone,callback) {
    merchantDAO.getMerchantByPhone(phone,callback);
}

MerchantService.prototype.getMerchantByPrivatePhone = function (phone,callback) {
    merchantDAO.getMerchantByPrivatePhone(phone,callback);
}

MerchantService.prototype.getAppId = function (appUrl,callback) {
    merchantDAO.getAppId(appUrl,function(appDetails,err){
        callback(appDetails,err);
    });
}


module.exports = MerchantService;