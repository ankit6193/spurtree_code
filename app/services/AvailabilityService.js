var MerchantDAO = require('./../dao/MerchantDAO');
var merchantDAO = new MerchantDAO();

var AvailabilityDAO = require('./../dao/AvailabilityDAO');
var availabilityDAO = new AvailabilityDAO();

function AvailabilityService() {

}

AvailabilityService.prototype.getAvailability = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone.toString(),function(merchant_details,err){
        if(merchant_details==null){
            merchant_availability_response = {"validate_status":0,"message":"Data not available"}
            callback(merchant_availability_response,err)
        }else{
        var data = {
            "merchant_id":merchant_details.merchant_id,
            "user_phone":req.query.merchant_phone.toString()
        }
        availabilityDAO.getAvailability(data,function(merchant_availability_response,err){
            callback(merchant_availability_response,err)
        });
        }
    });
}

AvailabilityService.prototype.setAvailability = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.body.merchant_phone,function(merchant_details,err){
        if(merchant_details){
            var data = {
                "merchant_id":merchant_details.merchant_id
            }
            for(var i=0;i<req.body.schedule.length;i++){
                data.dayoftheweek = req.body.schedule[i].dayoftheweek;
                data.start_hour = req.body.schedule[i].start_hour;
                data.start_min = req.body.schedule[i].start_min;
                data.end_hour = req.body.schedule[i].end_hour;
                data.end_min = req.body.schedule[i].end_min;
                data.is_available=req.body.schedule[i].is_available
               availabilityDAO.setAvailability(data,function(set_merchant_availability_response,err){
                   
               });
           }    
           if(i==req.body.schedule.length)  {
               set_merchant_availability_response = {"validate_status":1,"message":"Updated"}
               callback(set_merchant_availability_response,err)
           }
        }else{
            set_merchant_availability_response = {"validate_status":0,"message":"Merchant not found"}
            callback(set_merchant_availability_response,err)
        }
        
    });
}



module.exports = AvailabilityService;