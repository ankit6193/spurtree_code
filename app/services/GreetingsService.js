var GreetingsDAO = require('./../dao/GreetingsDAO');
var greetingsDAO = new GreetingsDAO();

var MerchantDAO = require('./../dao/MerchantDAO');
var merchantDAO = new MerchantDAO();

var config = require('config');
var uplaod_url = config.get('uplaod_url');
function GreetingsService() {

}

GreetingsService.prototype.getGreetings = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone,function(merchant_exist,err){
        if(merchant_exist){
        greetingsDAO.getGreetings(merchant_exist.merchant_id,req,function(greetings,err){
            if(greetings.length>0){
                greetings_response = {"validate_status":1,"message":"Merchant Found","greeting_type":"custom","greetings":greetings}
                callback(greetings_response,err);
            }else{
                greetingsDAO.getDefaultGreetings(req,function(default_greetings,err){
                    greetings_response = {"validate_status":1,"message":"Merchant Found","greeting_type":"default","greetings":default_greetings}
                    callback(greetings_response,err);
                });
            }
        });
        }else{
            greetings_response = {"validate_status":0,"message":"No merchant found"}
            callback(greetings_response,err)
        }
    });
    
}


GreetingsService.prototype.deleteGreetings = function (req,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone,function(merchant_exist,err){
        if(merchant_exist){
        greetingsDAO.deleteGreetings(merchant_exist.merchant_id,req,function(greetings,err){
            if(greetings==1){
                greetings_response = {"validate_status":1,"message":"Greeting Deleted Successfully"}
            }else{
                greetings_response = {"validate_status":1,"message":"No Greetings Available"}
            }
            callback(greetings_response,err);
        });
        }else{
            greetings_response = {"validate_status":0,"message":"No merchant found"}
            callback(greetings_response,err)
        }
    });
    
}


GreetingsService.prototype.setGreetings = function (req,filePath,callback) {
    merchantDAO.getMerchantByPhone(req.query.merchant_phone,function(merchant_exist,err){
        if(merchant_exist){
        
        var data = {
            "merchant_id":merchant_exist.merchant_id,
            "greeting_type":req.query.type,
            "greeting_url":uplaod_url+filePath+".mp3"
        }
        greetingsDAO.setGreetings(data,function(greetings,err){
            set_greetings_response = {"validate_status":1,"message":"Merchant Found","filePath":data.greeting_url,"type":"custom"}
            callback(set_greetings_response,err)
        });
        }else{
            set_greetings_response = {"validate_status":0,"message":"No merchant found"}
            callback(set_greetings_response,err)
        }
    });
    
}





// function uploadFile (req, res) {

//     if (req.headers['x-app-token'] == null) {
//     return res.json({ status: 400, msg: 'x-app-token missing', title: 'Authorization Failed' });
//     }
//     var app_key = utils.getTokenKey(req.headers['x-app-token']);
//     if(app_key === config.app_key.stella){
//     var s3 = new AWS.S3({ Bucket: config.app_key.bucket_name })
//     var storage = multerS3({
//     s3: s3,
//     acl: 'public-read-write',
//     bucket: config.app_key.bucket_name,
//     metadata: function (req, file, cb) {
//     cb(null, { fieldName:'file'});
//     },
//     key: function (req, file, cb) {
//     cb(null, app_key + '-' + Date.now() + path.extname(file.originalname))
//     }
//     })
//     var upload = multer({storage: storage }).single('file');
//     upload(req, res,function (err) {
//     if (err) {
//     console.log(err);
//     }
//     else {
//     res.status(200);
//     res.json({
//     status: 200,
//     urlPath:'https://s3.us-east-2.amazonaws.com/sttarter-useast-dev/'+req.file.key
//     });
//     }
//     });
//     }else{
//     var storage = multer.diskStorage({
//     destination: function (req, file, callback) {
//     callback(null, config.fileUpload.uploadPath);
//     },
//     filename: function (req, file, callback) {
//     callback(null, app_key + '-' + Date.now() + path.extname(file.originalname));
//     }
//     });
//     var upload = multer({ storage: storage }).single('file');
//     upload(req, res, function (err) {
//     if (err) {
//     console.log(err);
//     }
//     else {
//     if (req.headers.host === "ssl.sttarter.com:9443")
//     urlPathProtocol = "https";
//     else
//     urlPathProtocol = "http";
//     res.status(200);
//     res.json({
//     status: 200,
//     urlPath: urlPathProtocol + "://" + req.headers.host + config.fileUpload.downloadPath + req.file.filename
//     });
//     }
//     });
    
//     }
//     },



module.exports = GreetingsService;