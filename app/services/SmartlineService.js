var SmartlineDAO = require('./../dao/SmartlineDAO');
var smartlineDAO = new SmartlineDAO();
var MerchantService = require('./MerchantService');
var merchantService = new MerchantService();
var fs = require('fs');
var moment = require('moment')
function SmartlineService() {

}

SmartlineService.prototype.getcallstrategy = function (call_strategy_params,callback) {
    smartlineDAO.getcallstrategy(call_strategy_params,function (call_strategy, err) {
        var call_type = "";
        if(call_strategy==null){
            callback('{"select":"INVALID"}',err);
            return;
        }
        console.log("***********************",call_strategy_params.query.From,call_strategy.phone)
                if(call_strategy_params.query.From.includes(call_strategy.phone)){
                    getcallstrategy_response =  '{"select":"SELF"}';
                    call_type = "SELF";
                
                }else{
                    var currentHours = new Date(call_strategy_params.query.CurrentTime).getHours();
                    var currentMinutes = new Date(call_strategy_params.query.CurrentTime).getMinutes();

                    var startHours = call_strategy.merchant_schedule.start_hour;
                    var startMinutes = call_strategy.merchant_schedule.start_min;

                    var endHours = call_strategy.merchant_schedule.end_hour;
                    var endMinutes = call_strategy.merchant_schedule.end_min;



                    var currentTime= moment(currentHours+':'+currentMinutes, "HH:mm");
                    var startTime = moment(startHours+':'+startMinutes, "HH:mm");
                    var endTime = moment(endHours+':'+endMinutes, "HH:mm");
                    console.log("im new",currentTime,startTime,endTime,"******",currentTime.isBetween(startTime, endTime),!currentTime.isBetween(startTime, endTime))
                    console.log("Current Time",call_strategy_params.query.CurrentTime)
                    console.log("Current Data",currentHours,":",currentMinutes) 
                    console.log("Start Data",startHours,":",startMinutes)
                    console.log("End Data",endHours,":",endMinutes)
                   
                    console.log(currentTime.isBetween(startTime, endTime) , call_strategy.merchant_schedule.is_available!=0)
                    // if(currentHours>startHours && endHours<currentHours|| call_strategy.merchant_schedule.is_available==0){
                    if(currentTime.isBetween(startTime, endTime) && call_strategy.merchant_schedule.is_available!=0){
                            getcallstrategy_response =  '{"select":"ON"}';
                            call_type = "ON";                        
                    }else{
                            getcallstrategy_response =  '{"select":"OFF"}';
                            call_type = "OFF";
                    }
                    

                }

                console.log("******Call Strategy Response*************",getcallstrategy_response)
                    
                                    
                
                    merchantService.getMerchantByPhone(call_strategy_params.query.To,function(merchant,err){
                        if(err || merchant == null){
                            callback("False",err)
                        }else{
                            var data = {
                                "callSid" :call_strategy_params.query.CallSid,
                                "merchant_id" : merchant.merchant_id,
                                "contact_id" : call_strategy.phone,
                                "request_status" : call_strategy_params.query.DialCallStatus,
                                "call_status": call_strategy_params.query.CallType,
                                "recording_url" : call_strategy_params.query.RecordingUrl,
                                "call_initiation_time": call_strategy_params.query.StartTime,
                                "call_completion_time": call_strategy_params.query.EndTime,
                                "is_outgoing":0,
                            }
                            console.log("test",data)
                            if(call_type=="SELF"){
                                data.direction="outgoing";
                            }else{
                                data.direction="incoming";
                            }
                            smartlineDAO.getStatusToDisplay(data.direction,data.call_status,function(my_status,err){
                                console.log("**********************************",my_status.id)
                                if(err){
                                    callback("False",err)
                                }else{
                                    data.status_id = my_status.id;
                                    if(call_type != "SELF"){
                                        smartlineDAO.insertCallInfo(data,
                                            function(record_call_info,err){
                                            smartlineDAO.insertCallLog(call_strategy_params.query);
                                            callback(getcallstrategy_response,err);
                                            return;
                                        });
                                    }else{
                                        var dataSelf = {
                                            "merchant_id" : merchant.merchant_id,
                                            "callSid":call_strategy_params.query.CallSid,
                                            "request_status" : call_strategy_params.query.DialCallStatus,
                                            "call_status": call_strategy_params.query.CallType,
                                            "recording_url" : call_strategy_params.query.RecordingUrl,
                                            "call_initiation_time": call_strategy_params.query.StartTime,
                                            "call_completion_time": call_strategy_params.query.EndTime,
                                            "status_id" : my_status.id
                                            }
                                            console.log("test",data)
                                            smartlineDAO.updateCallInfoBlank(dataSelf,function(recorded,err){
                                                if(err){
                                                    callback('{"select":"INVALID"}',err);
                                                }else{
                                                        smartlineDAO.insertCallLog(call_strategy_params.query);
                                                        callback(getcallstrategy_response,err);
                                                        return;
                                                    }
                                            })
                                    }
                                }
                            })
                        }
                 });
                merchantService.getMerchantByPhone(call_strategy_params.query.To,function(merchant,err){
                    if(err || merchant == null){
                        callback('{"select":"INVALID"}',err);
                    }else{
                        var dataNew = {
                            "merchant_id": merchant.merchant_id,
                            "contact_phone": call_strategy_params.query.From
                        }
                        smartlineDAO.insertContacts(dataNew);
                    }})       
                
})

}

SmartlineService.prototype.getgreetingfile = function(call_greeting_params,callback){
    var getgreetings_response = "";
    smartlineDAO.getgreetingfile(call_greeting_params,
        function(call_greeting,err){
            if(call_greeting==null){
                smartlineDAO.getdefalutgreetingfile(call_greeting_params,function(default_greeting,err){
                    getgreetings_response = default_greeting.greeting_url;
                    callback(getgreetings_response,err) 
                });
            }else{
                getgreetings_response = call_greeting.merchant_greeting.greeting_url;
                callback(getgreetings_response,err) 
            }
        console.log("******Get Greeting Response*************",getgreetings_response)
        smartlineDAO.insertCallLog(call_greeting_params.query);       
    });
}

SmartlineService.prototype.getCallType = function(call_strategy_params,callback){
    smartlineDAO.getcallstrategy(call_strategy_params,function (call_strategy, err) {
        var call_type = "";
        if(call_strategy==null){
            callback('{"select":"INVALID"}',err);
            return;
        }else{
            if(call_strategy_params.query.From.includes(call_strategy.phone)){
                call_type = "SELF";
            
            }else{
                call_type = "ONOFF"
            }
            callback(call_type,err);
        }
    });
}

var NewSmartlineService = new SmartlineService();


SmartlineService.prototype.getnumbertodial = function(number_to_dial_params,callback){
        var number_to_dial_response ="";
            merchantService.getMerchantByPhone(number_to_dial_params.query.To,function(merchant,err){  
                if(err || merchant == null){
                    console.log("hi")
                    callback("Fail",err)
                }else{

                    NewSmartlineService.getCallType(number_to_dial_params,function(call_strategy, err){
                            console.log("******************My_Type****************",call_strategy)
                            smartlineDAO.insertCallLog(number_to_dial_params.query); 
                            if(call_strategy!="SELF"){
                                    merchantService.getMerchantByPhone(number_to_dial_params.query.To,function(merchant,err){  
                                        if(err || merchant == null){
                                            callback("Fail",err)
                                        }else{
                                            smartlineDAO.getMerchant(merchant,function(number_to_dial,err){      
                                            number_to_dial_response = number_to_dial.phone.toString()        
                                            console.log("******************number_to_dial_response*****************",number_to_dial_response)
                                            callback(number_to_dial_response,err);
                                            smartlineDAO.insertCallLog(number_to_dial_params.query); 
                                            var dataSelf = {
                                                "callSid":number_to_dial_params.query.CallSid,
                                                "contact_id" : number_to_dial_params.query.From
                                                }
                                                smartlineDAO.updateCallInfo(dataSelf,function(recorded,err){
                                                    if(err){
                                                        callback('Fail',err);
                                                    }
                                                })
                                            });
                                        }
                                    })
                            }else{
                                smartlineDAO.getnumbertodialTo(number_to_dial_params.query.CallSid,function(number_to_dial,err){      
                                    number_to_dial_response = number_to_dial.contact_id.toString()        
                                    console.log("******************number_to_dial_response*****************",number_to_dial_response)
                                    callback(number_to_dial_response,err);
                                });
                            }
                        })
            }
        }); 
}


SmartlineService.prototype.recordcallinfo = function(record_call_info_params,callback){
    merchantService.getMerchantByPhone(record_call_info_params.query.To,function(merchant,err){
        if(err || merchant == null){
            callback("Fail",err)
        }else{
            
                    var data = {
                        "callSid" :record_call_info_params.query.CallSid,
                        "merchant_id" : merchant.merchant_id,
                        "request_status" : record_call_info_params.query.DialCallStatus,
                        "call_status": record_call_info_params.query.CallType,
                        "recording_url" : record_call_info_params.query.RecordingUrl,
                        "call_initiation_time": record_call_info_params.query.StartTime,
                        "call_completion_time": record_call_info_params.query.EndTime,
                    }
                    console.log("test",data)
                        smartlineDAO.getCallSid(data,function(is_callsid,err){
                            // *******************************
                            var direction = "";
                            if(is_callsid.is_outgoing==0){
                                direction = "incoming";
                            }else{
                                direction = "outgoing";
                            }
                            smartlineDAO.getStatusToDisplay(direction,record_call_info_params.query.CallType,function(my_status,err){
                                if(err){
                                    callback("False",err)
                                }else{
                                    console.log("********************************************************status",my_status.id)
                                    data.status_id = my_status.id;
                                        if(is_callsid==null){
                                            smartlineDAO.insertCallInfo(data,
                                                function(record_call_info,err){
                                                console.log("******Get Record Call Info Response*************INSERT",record_call_info);
                                                smartlineDAO.insertCallLog(record_call_info_params.query);
                                                record_call_info = "True";
                                                callback(record_call_info,err);
                                            });
                                        }else{
                                            smartlineDAO.updateCallInfo(data,
                                                function(record_call_info,err){
                                                    if(record_call_info){
                                                        record_call_info = "True"
                                                    }else{
                                                            record_call_info = "False"
                                                    }
                                                console.log("******Get Record Call Info Response*************UPDATE",record_call_info);
                                                smartlineDAO.insertCallLog(record_call_info_params.query);
                                                callback(record_call_info,err);
                                            });
                                        }
                                    }
                                });
                            // *******************************
                        })
                    
            //     }
            // });        
        }


    });
}


module.exports = SmartlineService;