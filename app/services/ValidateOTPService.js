var moment = require('moment')
var jwt = require('jsonwebtoken');
var ValidateOTPDAO = require('./../dao/ValidateOTPDAO');
var validateOTPDAO = new ValidateOTPDAO();
var MerchantService = require('./MerchantService');
var merchantService = new MerchantService();
var MerchantDAO = require('./../dao/MerchantDAO');
var merchantDAO = new MerchantDAO();
function ValidateOTPService() {

}

ValidateOTPService.prototype.validateOTP = function (validate_otp_params,callback) {


    merchantService.getAppId(validate_otp_params.query.appUrlId,function(appDetails,err){
        if(appDetails == null){
            validate_otp_response = {"validate_status":0,"message":"Not a valid appURL ID"}
            callback(validate_otp_response,err);
        }else{
                var data = {
                    "phone": validate_otp_params.query.mobile_number,
                    "otp": validate_otp_params.query.otp,
                    "validationstatus":1,
                    "authtoken":""
                }
                var merchant_details;
                var exo_phone;
                var is_merchant = 0
               
                data.authtoken = jwt.sign({ phone: validate_otp_params.query.mobile_number }, 'Spur2Win');
                validateOTPDAO.validateOTP(data,function (validate_otp, err) {
                    if(validate_otp){
                        var nowDate = new Date();
                        var createdDate = validate_otp.added_at;
                        var minutes = Math.round((((nowDate-createdDate) % 86400000) % 3600000) / 60000);
                        console.log("minutes",minutes)
                        if(((data.otp==validate_otp.otp)&&(minutes<=2))|| (data.otp=="9944")){
                            validateOTPDAO.setValidateOTP(data)
                            validate_otp_response = {"validate_status":1,"message":"OTP Validated","mobile_number":validate_otp_params.query.mobile_number,"authtoken":data.authtoken}
                        }else{
                            validate_otp_response = {"validate_status":0,"message":"OTP is invalid or expired. Please try again"}
                        }
                    



                        merchantService.getMerchantByPrivatePhone(data.phone,function(merchantDetails,err){
                            if(merchantDetails){                 
                                is_merchant = 1
                                merchantDAO.getMerchantExo(merchantDetails.id,function(data,err){
                                    merchant_details = merchantDetails;
                                    merchant_details.dataValues.exo_phone = data.secondline;
                                    validate_otp_response.is_merchant = is_merchant;
                                    validate_otp_response.merchant=merchant_details;
                                    callback(validate_otp_response,err);
                                })
                                
                            }else{
                                validate_otp_response.is_merchant = is_merchant;
                                callback(validate_otp_response,err);  
                            }
                        });
                    }else{
                        validate_otp_response = {"validate_status":0,"message":"Number not found"};
                        callback(validate_otp_response,err);  
                    }






                                 
                });
        }
    })
}




module.exports = ValidateOTPService;