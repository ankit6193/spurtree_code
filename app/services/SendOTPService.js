var request = require('request');
var moment = require('moment')
var otpGenerator = require('otp-generator')
var FormData = require('form-data');
var MerchantService = require('./MerchantService');
var merchantService = new MerchantService();

var SendOTPDAO = require('./../dao/SendOTPDAO');
var sendOTPDAO = new SendOTPDAO();

function SendOTPService() {

}

SendOTPService.prototype.sendOTP = function (send_otp_params,callback) {
    merchantService.getAppId(send_otp_params.query.appUrlId,function(appDetails,err){
        if(appDetails == null){
            send_otp_response = {"validate_status":0,"message":"Not a valid appURL ID"}
            callback(send_otp_response,err);
        }else{
            var my_otp = otpGenerator.generate(4, {digits:true,alphabets :false,upperCase  :false,specialChars  :false});
            var nowDate = new Date();
            console.log(nowDate)
            var data = {
                "phone": send_otp_params.query.mobile_number,
                "otp":my_otp,
                "validationstatus":0,
                "added_at" :nowDate,
                "app_id" : appDetails.id
            }
            sendOTPDAO.sendOTP(data,function (send_otp, err) {
                var dataString = {
                    From:"09513886363",
                    To: data.phone,
                    Body:"Your OTP is "+my_otp+ " it will expire in 2 minutes"
                };
                
                var options = {
                    url: 'https://exotel256:341a28248abc0891c0de30474b586acc9f8f2c56@api.exotel.com/v1/Accounts/exotel256/Sms/send.json',
                    form: dataString,
                
                };
                function request_callback(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        send_otp_response = {"validate_status":1,"message":"OTP sent successfully..!!","mobile_number":send_otp_params.query.mobile_number}
                        
                    }else{
                        message = JSON.parse(response.body)
                        send_otp_response = {"validate_status":0,"message":"OTP not sent..!!","mobile_number":send_otp_params.query.mobile_number,"message":message}
                        
                    }
                    callback(send_otp_response,err);
                }
            request.post(options, request_callback);
                
            
            });
        }
    })    
}




module.exports = SendOTPService;