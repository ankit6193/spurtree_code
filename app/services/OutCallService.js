var OutCallDAO = require('./../dao/OutCallDAO');
var outCallDAO = new OutCallDAO();

var MerchantService = require('./../services/MerchantService');
var merchantService = new MerchantService();

function OutCallService() {

}

OutCallService.prototype.makeACall = function (req,callback) {
    
    merchantService.getAppId(req.query.appUrlId,function(appDetails,err){
        if(appDetails == null){
            out_call_response = {"validate_status":0,"message":"Not a valid appURL ID"}
            callback(out_call_response,err);
        }else{
    
            var dateNow = new Date().toISOString();
            merchantService.getMerchantByPhone(req.query.exoPhone,function(merchant,err){
                if(err || merchant == null){
                    out_call_response = {"validate_status":0,"message":"Merchant not found"}
                    callback(out_call_response,err)
                }else{

                    var data = {
                        "merchant_id": merchant.merchant_id,
                        "contact_id": req.query.dialTo.replace(/ +/g, ""),
                        "is_outgoing":1,
                        "call_initiation_time": dateNow,
                        "app_id":appDetails.id
                    }
                    console.log(data)
                    outCallDAO.makeACall(data,function(called_resp,err){
                        if(err||called_resp==null){
                            out_call_response = {"validate_status":0,"message":"No call initiated"};
                            callback(out_call_response,err);
                            return;
                        }else{
                            out_call_response = {"validate_status":1,"data":called_resp};
                            callback(out_call_response,err);
                            return;
                        }
                    });
                }
            });
        }
    });
    
}



module.exports = OutCallService;