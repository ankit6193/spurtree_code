var UserDAO = require('./../dao/UserDAO');
var userDAO = new UserDAO();

function UserService() {

}

UserService.prototype.getUsers = function (callback) {
    userDAO.getUsers(callback);
}

UserService.prototype.getUserById = function (id,callback) {
    userDAO.getUserById(id,callback);
}



module.exports = UserService;