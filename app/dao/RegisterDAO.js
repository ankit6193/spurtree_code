// Require DB  conncetion model 
const db = require('../models/models')

var config = require('config');
var mysqlConfig = config.get('mysql');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(mysqlConfig.database, mysqlConfig.username, mysqlConfig.password, {
    host: mysqlConfig.host,
    dialect: mysqlConfig.dialect,
    pool: {
        min: mysqlConfig.min,
        max: mysqlConfig.max,
        idle: mysqlConfig.idle
    }
});

function RegisterDAO() {
}

RegisterDAO.prototype.registermerchants = function (data,callback) {
    db.merchant.create(data,{
    }).then(callback)
    
}

RegisterDAO.prototype.addmerchantsSecondLine = function (data) {
    var insert_row = {
        "merchant_id": data.id,
        "secondline": data.secondline,
        "status" :1,
        "telco_circle_id":data.telco_region_code
    }
    db.second_lines.create(insert_row,{
    })
    
}

RegisterDAO.prototype.addSchedules = function (data) {
    sequelize.query('CALL insert_schedule_merchant('+data+')', function (err, rows, fields) {
        connection.release();
        if (err) {
           res.status(400).send(err);
       }
       res.status(200).send(rows);
   }); 
}


RegisterDAO.prototype.addmerchantsDevice = function (data) {
    var insert_row = {
        "merchant_id": data.id,
        "platform": data.platform,
        "device_id" :data.device_id
    }
    db.merchant_device.create(insert_row,{
    })
    
}

module.exports = RegisterDAO;