// Require DB  conncetion model 
const db = require('../models/models')


function SmartlineDAO() {
}

SmartlineDAO.prototype.getcallstrategy = function (call_strategy_params,callback) {
    var today = new Date ().getDay()+1;

    db.merchant.findOne({
        include: [{model:db.merchant_schedule,where:{dayoftheweek:today}},{model:db.second_lines,where:{secondline:call_strategy_params.query.To}}],
    }).then(callback)
}

SmartlineDAO.prototype.getgreetingfile = function (call_greeting_params,callback) {
    
    db.merchant.findOne({
        include: [{model:db.merchant_greetings,where:{greeting_type:call_greeting_params.query.Type}},{model:db.second_lines,where:{secondline:call_greeting_params.query.To}}],
    }).then(callback)
    
}

SmartlineDAO.prototype.getdefalutgreetingfile = function (call_greeting_params,callback) {
    
    db.merchant_greetings.findOne({
        where : [{greeting_type:"DEFAULT"+call_greeting_params.query.Type}]
    }).then(callback)
    
}

SmartlineDAO.prototype.getnumbertodialTo = function (call_sid,callback) {
    db.call_master.findOne({
        where:[{callSid:call_sid}],
    }).then(callback)
    
}

SmartlineDAO.prototype.getselfnumbertodial = function (number_to_dial_params,id,callback) {
    db.call_master.findOne({
        where:[{merchant_id:id,callSid:number_to_dial_params.query.CallSid,is_outgoing:1}]
    
    }).then(callback)
}

SmartlineDAO.prototype.getCallSid = function (data,callback) {
    db.call_master.findOne({
        where:{callSid:data.callSid}
    }).then(callback)
    
}

SmartlineDAO.prototype.updateCallInfo = function (data,callback) {
    db.call_master.update(data,{
        where:{callSid:data.callSid}
    }).then(callback)
    
}
SmartlineDAO.prototype.updateCallInfoBlank = function (incoming_data,callback) {
    var data = incoming_data
    db.call_master.findOne({
        where:{is_outgoing:1,merchant_id:data.merchant_id,callSid:null},
        order : [['created_at','DESC']]
    }
    ).then(function (findData){ 
        var update_data = {
            "callSid" :data.callSid,
            "request_status" : data.request_status,
            "call_status": data.call_status,
            "recording_url" : data.recording_url,
            "call_initiation_time": data.call_initiation_time,
            "call_completion_time": data.call_completion_time,
        }
        db.call_master.update(update_data,{
        where:{id:findData.id}
    }).then(callback)
    })    
}
SmartlineDAO.prototype.insertCallInfo = function (data,callback) {
    db.call_master.upsert(data,{
        where:{callSid:data.CallSid}
    }).then(callback)
    
}

SmartlineDAO.prototype.insertCallLog = function (data) {
    db.call_history_log.upsert(data,{
        where:{CurrentTime:data.CurrentTime}
    })
    
}


SmartlineDAO.prototype.insertContacts = function (data) {
    db.merchant_contacts.findOrCreate({
        where:data
    })
    
}

SmartlineDAO.prototype.getMerchant = function (data,callback) {
    db.merchant.findOne({
        where:{id:data.merchant_id}
    }).then(callback)
}

SmartlineDAO.prototype.getStatusToDisplay = function (direction,status,callback){

    db.call_status_display.findOne({
        where : {direction:direction,incoming_status:status},
        attribute : ['id', 'status_name', 'direction', 'incoming_status']
    }).then(callback)
}

module.exports = SmartlineDAO;