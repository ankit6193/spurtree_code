// Require DB  conncetion model 
const db = require('../models/models')


function MerchantDAO() {
}

MerchantDAO.prototype.getMerchantById = function (req,callback) {
    var merchant_id = req.params.id || req.id
    db.merchant.findOne({
        where:{id:merchant_id}
    }).then(callback)
}



MerchantDAO.prototype.getMerchantByPhone = function (phone,callback) {
    var merchant_phone = phone
    db.second_lines.findOne({
        where:{secondline:merchant_phone}  
    }).then(callback)
}


MerchantDAO.prototype.getMerchantByPrivatePhone = function (phone,callback) {
    var merchant_private_phone = phone
    db.merchant.findOne({
        where:{phone:merchant_private_phone},  
    }).then(callback)
}

MerchantDAO.prototype.getMerchantExo = function (id,callback) {
    
    db.second_lines.findOne({
        where:{merchant_id:id},  
    }).then(callback)
}

MerchantDAO.prototype.getAppId = function (appUrlId,callback) {
    db.app_identifier.findOne({
        where:{app_url_id:appUrlId},  
    }).then(callback)
}

module.exports = MerchantDAO;