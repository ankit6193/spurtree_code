// Require DB  conncetion model 
const db = require('../models/models')


function AvailabilityDAO() {
}

AvailabilityDAO.prototype.getAvailability = function (data,callback) {
    var merchant_id = data.merchant_id 
    db.merchant_schedule.findAll({
        where:{merchant_id:merchant_id},
        order: [['dayoftheweek', 'ASC']]
    }).then(callback)
}

AvailabilityDAO.prototype.setAvailability = function (data,callback) {
    db.merchant_schedule.update(data,{
        where:{merchant_id:data.merchant_id,dayoftheweek:data.dayoftheweek}
    }).then(callback)
}



module.exports = AvailabilityDAO;