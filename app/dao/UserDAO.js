// Require DB  conncetion model 
const db = require('../models/models')


function UserDAO() {
}

UserDAO.prototype.getUsers = function (callback) {
    db.users.findAll({
        attributes:['id','name']
    }).then(callback)
}
UserDAO.prototype.getUserById = function (id,callback) {
    db.users.findOne({
        where:{id:id},
        attributes:['id','name']
    }).then(callback)
}

module.exports = UserDAO;