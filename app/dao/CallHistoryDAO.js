// Require DB  conncetion model 
const db = require('../models/models')


function CallHistoryDAO() {
}

CallHistoryDAO.prototype.getcallHistoryByUser = function (data,callback) {
    var merchant_id = data.merchant_id 
    var contact_number = data.user_phone.slice(-10);
    db.sequelize.query("select * from call_master as cm inner join call_status_display as cs on cm.status_id=cs.id where merchant_id = "+merchant_id+" and contact_id  like '%"+contact_number +"'").then(callback)

    // db.call_master.findAll({
    //     where:{merchant_id:merchant_id,contact_id:data.user_phone},
    //     include:[{model:db.call_status_display}]
    // }).then(callback)
}

CallHistoryDAO.prototype.getcallHistory = function (data,callback) {
    var merchant_id = data.merchant_id 
    db.sequelize.query("select * from  call_master as cm left join call_status_display as cs on cm.status_id = cs.id where cm.updated_at in (select max(updated_at) from call_master where merchant_id = "+merchant_id+" group by SUBSTR(contact_id ,-10) ) order by cm.created_at desc limit 20").then(callback)
    // db.call_master.findAll({
    //     order:[['updated_at', 'DESC']],
    //     group:['contact_id']

    // }).then(callback)
}

CallHistoryDAO.prototype.getLastCall = function (data,callback) {
    var merchant_id = data.merchant_id 
    db.call_master.findOne({
        limit :1,
        order : [['updated_at','DESC']],
        where:{merchant_id:merchant_id,is_outgoing:0}
    }).then(callback)
}


module.exports = CallHistoryDAO;