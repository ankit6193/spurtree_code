// Require DB  conncetion model 
const db = require('../models/models')




function GreetingsDAO() {
}

GreetingsDAO.prototype.getGreetings = function (m_id,req,callback) {
    db.merchant_greetings.findAll({
        where:{merchant_id:m_id,greeting_type:req.query.greeting_type}
    }).then(callback)
}

GreetingsDAO.prototype.getDefaultGreetings = function (req,callback) {
    db.merchant_greetings.findAll({
        where: {greeting_type: "DEFAULT"+req.query.greeting_type}
    }).then(callback)
}

GreetingsDAO.prototype.deleteGreetings = function (m_id,req,callback) {
    db.merchant_greetings.destroy({
        where:{merchant_id:m_id,greeting_type:req.query.greeting_type}
    }).then(callback)
}

GreetingsDAO.prototype.setGreetings = function (data,callback) {
    console.log(data)
    db.merchant_greetings.findOne({
        where:{merchant_id:data.merchant_id,greeting_type:data.greeting_type}  
    }).then(function(my_data){
        if(my_data){
            db.merchant_greetings.update(data,{
                where:{id:my_data.id}  
            }).then(callback)
        }else{
            db.merchant_greetings.create(data,{
            }).then(callback)
        }
    })
}


module.exports = GreetingsDAO;